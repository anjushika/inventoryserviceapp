package com.classpath.inventoryservice.service;

import com.classpath.inventoryservice.model.Item;
import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SimpleSourceBean {
    private final Source source;

    public void publishItem(Item item){
        this.source.output().send(MessageBuilder.withPayload(item).build());
    }
}